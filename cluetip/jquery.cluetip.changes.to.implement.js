//1. New possible settings:
var blah = {
  positionBy: 'set', 
offsetHorizontal: 0, 
offsetVertical: 22
};
//somewhere around line 220
      if (defaults.positionBy=='set'){ //Modification by C.M. 27.8.2007 OPTION positionBy: 'set'
        linkTop = posY = $this.offset().top+defaults.offsetVertical;
        linkLeft = $this.offset().left+defaults.offsetHorizontal;
      } else {//END Modification by C.M. 27.8.2007
       	linkTop = posY = $this.offset().top;
 	linkLeft = $this.offset().left;
      }

     if (defaults.positionBy=='set'){ //Modification by C.M. 27.8.2007 OPTION positionBy: 'set'
      	winWidth = $(window).width();
	    if ($this[0].tagName.toLowerCase() != 'area') {
 	        sTop = $(document).scrollTop();
 	  // position clueTip horizontally
      		posX = (linkLeft + tipWidth < winWidth) ? linkLeft : winWidth - tipWidth - 20;
      		$cluetip.removeClass().css({width: defaults.width});
		}
	    posX < linkLeft ? $cluetip.addClass('clue-left-' + ctClass).removeClass('clue-right-' + ctClass)
	    : $cluetip.addClass('clue-right-' + ctClass).removeClass('clue-left-' + ctClass);
      	$cluetip.css({left: posX });
      } else {//END Modification by C.M. 27.8.2007
	    mouseX = event.pageX;
	    mouseY = event.pageY;
	    if ($this[0].tagName.toLowerCase() != 'area') {
	      sTop = $(document).scrollTop();

	// position clueTip horizontally
 		  posX = (linkWidth > linkLeft && linkLeft > tipWidth)
		     || linkLeft + linkWidth + tipWidth > winWidth
		     ? linkLeft - tipWidth - 15
		     : linkWidth + linkLeft + 15;

  	      $cluetip.removeClass().css({width: defaults.width});
	      if ($this.css('display') == 'block' || $this[0].tagName.toLowerCase() == 'area' || defaults.positionBy == 'mouse') { // position by mouse
	        if (mouseX + 20 + tipWidth > winWidth) {
	          posX = (mouseX - tipWidth - 20) >= 0 ? mouseX - tipWidth - 20 :  mouseX - (tipWidth/2);
	        } else {
	          posX = mouseX + 20;
	        }
	        var pY = posX < 0 ? event.pageY + 20 : event.pageY;
	      }
       }
	   posX < linkLeft ? $cluetip.addClass('clue-left-' + ctClass).removeClass('clue-right-' + ctClass)
	   : $cluetip.addClass('clue-right-' + ctClass).removeClass('clue-left-' + ctClass);
	   $cluetip.css({left: (posX > 0) ? posX :( mouseX + (tipWidth/2) > winWidth) ? winWidth/2 - tipWidth/2 : Math.max(mouseX - (tipWidth/2),0)});
     }


//somewhere around line 250	  
	  ajaxSettings.beforeSend = function() {
            if (defaults.waitImage) {
             if (defaults.positionBy=='set'){ //Modification by C.M. 27.8.2007 OPTION positionBy: 'set'
 	     	  	  $('#cluetip-inner').empty(); //Modification by C.M. 30.8.2007 No Old Tooltips!
			      $('#cluetip-waitimage')
	              	.css({top: posY, left: posX})
	              	.show();
	          }
              else {//END Modification by C.M. 27.8.2007
	              $('#cluetip-waitimage')
	              .css({top: mouseY-10, left: parseInt(posX+(tipWidth/2),10)})
	              .show();
	          }
            }
          };

//remark: .empty() prevents showing up of old tooltips which could confuse users when i rolled over one icon (with a sticky tooltip) and then right after that to the next, the old tooltip showed up instead of the load image until the new on was loaded.

          ajaxSettings.complete = function() {
            $('#cluetip-waitimage').hide();//Modification by C.M. 30.8.2007
            cluetipShow(pY);
           };
          $.ajax(ajaxSettings);

//remark: .hide() has advantages to .css({visibility = 'hdden'}) setting the css properties caused the waitimage to show up only once. 
// when i moved on to the next icon it didn't show up anymore. now it does.

